<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/UserList', 'UserController@index');
Route::post('/UserList/update/{id}', 'UserController@update'); 

// Route pour les reservations
/* Route::get('/reservation', 'ReservationController@index');
Route::get('/reservation/filter', 'ReservationController@filter');
Route::post('/reservation/new', 'ReservationController@store');
Route::post('/reservation/edit/{id}', 'ReservationController@update');
Route::delete('/reservation/{reservation}', 'ReservationController@destroy');
 */



//Route::resource('salles', 'SalleController');
