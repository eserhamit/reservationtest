<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css')}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">


         
       
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <router-link to="/home" class="nav-link">Home</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/user" class="nav-link">Users</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/salle" class="nav-link">Salles</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link to="/reservation" class="nav-link">Reservation</router-link>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="container">
                <!-- component matched by the route will render here -->
                <router-view></router-view>
            </div>
        </div>
         <script src="{{ asset('js/app.js')}}"></script>
    </body>
</html>
