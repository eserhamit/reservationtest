/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Home from './components/HomeComponent.vue';
import User from './components/UserComponent.vue';
import Salle from './components/SalleComponent.vue';

import Reservation from './components/ReservationComponent.vue';
 
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';

import locale from 'view-design/dist/locale/fr-FR';
Vue.use(ViewUI, {locale: locale});



import { method } from 'lodash';

const  routes = [
    {
        path: '/home',
        component:Home
    },
    {
        path: '/user',
        component: User
    },
    {
        path: '/salle',
        component: Salle
    },
    {
        path: '/reservation',
        component: Reservation
    }
];

const router = new VueRouter({routes});

 

const app = new Vue({
    el: '#app',
    router: router,
    methods: {},
});
