<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Salle;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ReservationResource;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;


class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* $reservation = DB::table('reservations')
        ->join('salles', 'salles.id', '=', 'reservations.salle_id') 
        ->select('salles.*', 'reservations.*')
        ->get();

        
        return response()->json(['reservation' => $reservation]); */
        $user= Auth::id();

        $reservation = ReservationResource::collection(Reservation::
        join('salles', 'salles.id', '=', 'reservations.salle_id')
        ->select('salles.*', 'reservations.*')
        ->get());

        return response()->json([
            'data' => $reservation,
            //'message' => $request->all(),
            'user' => $user,
            'status' => Response::HTTP_ACCEPTED
        ]);

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $new_reservation = Reservation::create($request->all());
        return response()->json([
            'data' => new ResevationResource($new_reservation),
            'message' => 'Successfully added',
            'status' => Response::HTTP_CREATED
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_calendar = Reservation::create($request->all());
        return response()->json([
            'data' => new ReservationResource($new_calendar),
            'message' => 'Successfully added new event!',
            'status' => Response::HTTP_CREATED
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
        return response($reservation, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    public function update(Request $request, Reservation $calendar)
    {
       /*  $reservation = $id;
        $reservation->title = $request->reservation['title'];
        $reservation->start = $request->reservation['start'];
        $reservation->end = $request->reservation['end'];

        //$reservation->save();
        $reservation = Reservation::where('id', '2')->first()->update(
            ['title' => $request->reservation['title']],
            ['start' => '2020-09-01 13:00:00'],
            ['end' => '2020-09-01 13:00:00']
            )
        ;
       return response()->json([$id, $reservation]); 

        $request->start = date("Y-m-d H:i:s", strtotime($request->start));
        $request->end = date("Y-m-d H:i:s", strtotime($request->end));
*/

       $calendar->update($request->all());
        return response()->json([
            'data' => new ReservationResource($calendar),
            'message' => $request->all(),
            'status' => Response::HTTP_ACCEPTED
        ]);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $calendar)
    {
        $calendar->delete();
        return response('Event removed successfully!', Response::HTTP_NO_CONTENT);
    }
}
