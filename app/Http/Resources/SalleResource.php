<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SalleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->numero,
            'etage' => $this->etage,
            'secteur' => $this->secteur,
            'batiment' => $this->batiment,
            'nbPlaces' => $this->nbPlaces,
            'projecteur' => $this->projecteur,
        ];
    }
}
