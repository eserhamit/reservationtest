<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'numero' => $this->numero,
            'etage' => $this->etage,
            'secteur' => $this->secteur,
            'batiment' => $this->batiment,
            'nbPlaces' => $this->nbPlaces,
            'projecteur' => $this->projecteur,
            'resourceId' => $this->salle_id,
            'user_id' => $this->user_id,
            'title' => $this->title,
            'start' => $this->start,
            'end' => $this->end
        ];
    }
}
