<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
  protected $fillable = [
     'batiment', 'numero', 'etage', 'secteur', 'nbPlaces', 'projecteur',
  ];
    //
    public function reservationsSalles() {
        return $this->hasMany('App\Reservations');
      }
}
