<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
      'id', 'salle_id', 'user_id', 'title', 'start', 'end',
    ];
    //
    public function user() {
        return $this->belongsTo('App\User');
    }
    public function salle() {
        return $this->belongsTo('App\Salle');
    }

}
